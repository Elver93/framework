import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch, NavLink, Redirect } from "react-router-dom";
import './App.css';
import Home from './Home';
import Login from './Login';
import Add from './Add';
import Reg from './Reg';
import Question from './Question';
import AuthService from './AuthService';

class App extends Component {
  api = "/api/";
  constructor(props) {
    super(props);

    this.Auth = new AuthService(`${this.api}users/authenticate`);
    this.state = {
      loggedIn: this.Auth.loggedIn(),
      questions: [],
      question: ""
    }

    this.questionsGet();
  }

  isLoggedIn =() =>{
    this.setState({loggedIn: !this.state.loggedIn})
  }

  questionsGet = () => {
    fetch(`${this.api}question`)
        .then(res => res.json())
        .then(questions => this.setState({questions: questions}))
  }

  questionGet =(id) => {
    fetch(`${this.api}question/` + id)
        .then(res => res.json())
        .then(questions => this.setState({question: questions}))
  }


  loginCreate = (message) => {
    this.Auth.fetch(`${this.api}users`,{
      method: "POST",
      body: JSON.stringify(message)
    })
  }

  questionAdd = (message) => {
    this.Auth.fetch(`${this.api}question`,{
      method: "POST",
      body: JSON.stringify(message)
    }).then(() => this.questionsGet())
  }

  answerAdd = (message, id) => {
    this.Auth.fetch(`${this.api}question/` + id,{
      method: "POST",
      body: JSON.stringify(message)
    }).then(() => this.questionGet(id))
  }

  answerVote = (id, answerId) => {
    this.Auth.fetch(`${this.api}question/${id}/${answerId}`,{
      method: "PUT"
    }).then(() => this.questionGet(id))
  }

  questionView = (id) => {
    this.Auth.fetch(`${this.api}question/${id}`,{
      method: "PUT"
    }).then(() => {
      this.questionGet(id)
      this.questionsGet()
    })
  };

  render() {
    let hideMenu = !this.Auth.loggedIn() ?
        <>
          <li className="nav-item">
            <NavLink exact activeClassName="active" className="nav-link" to="/login">Login</NavLink>
          </li>
          <li className="nav-item">
            <NavLink exact activeClassName="active" className="nav-link" to="/reg">Register</NavLink>
          </li>
        </>:null

    let showUser = this.Auth.loggedIn() ?
        <form className="form-inline my-2 my-lg-0">
          <label>Logged in as {this.Auth.getUserInfo().username}</label> &nbsp;&nbsp;
          <button className="btn btn-outline-success my-2 my-sm-0" onClick={this.Auth.logout}>Logout</button>
        </form>:null



    return (
        <Router>
      <div className="wrapper">
        <nav className="navbar navbar-expand-lg navbar-light bg-light header">
          <Link className="navbar-brand" to="/home">StackRipOff</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <NavLink exact activeClassName="active" className="nav-link" to="/home">Home</NavLink>
              </li>
              {hideMenu}
            </ul>
            {showUser}
          </div>
        </nav>

        <Switch>

          <Route path="/home" render={(props) => <Home {...props} questions={this.state.questions} questionView={this.questionView} />} />
          <Route path="/question/:id" render={(props) => <Question {...props}
                        id={props.match.params.id}
                        Auth={this.Auth}
                        answerVote={this.answerVote}
                        question={this.state.question}
                        questionsGet={this.questionsGet}
                        questionGet={this.questionGet}
                        answerAdd={this.answerAdd} />} />
          <Route path="/login" render={(props) => <Login {...props} Auth={this.Auth} setLogin={this.isLoggedIn} />} />
          <Route path="/reg" render={(props) => <Reg {...props} loginCreate={this.loginCreate} />} />
          <Route path="/add" render={(props) => <Add {...props} questionAdd={this.questionAdd} Auth={this.Auth} />} />
          <Redirect from="/" to="/home" />
        </Switch>

        <div className="foot">
          <p className="align-items-center">&copy; StackRipOff </p>
        </div>
      </div>
        </Router>
    );
  }
}

export default App;
