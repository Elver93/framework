import React, { Component } from 'react';

class Reg extends Component {
    constructor(props) {
        super(props);

    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        if (this.password.value === this.repassword.value)
        {

            let add = {
                username: this.username.value,
                password: this.password.value,
                mail: this.mail.value
            };
            this.props.loginCreate(add)
        }
        else{
            this.err.innerHTML = "Password does not match";
        }
    };


    render() {
        return (
            <div className="container cont">
                <h1>New User</h1>

                <div className="row justify-content-center align-items-center">
                    <div className="col-4">
                        <div className="card">
                            <div className="card-body">
                                <form action="" autoComplete="off">
                                    <div className="form-group">
                                        <input type="text" className="form-control" ref={username => this.username = username } placeholder="Name" />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" className="form-control" ref={password => this.password = password } placeholder="Password" />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" className="form-control" ref={repassword => this.repassword = repassword } placeholder="Match Password" />
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control" ref={mail => this.mail = mail } placeholder="Email" />
                                    </div>
                                    <button type="submit" onClick={this.handleFormSubmit} className="btn btn-primary col-12">Login</button>
                                    <br/>
                                    <labal ref={err => this.err = err} />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Reg;