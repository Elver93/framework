import React, { Component } from 'react';

class Login extends Component {
    constructor(props) {
        super(props);

    }

    login =() =>{
        this.props.Auth.login(this.username.value, this.password.value).then(res => {
            this.props.setLogin()
            this.props.history.push(`/home`)
        }).catch(err => this.err.innerHTML = "Wrong Password")
    }

    render() {
        return (
            <div className="container cont">
                <h1>Login</h1>

                <div className="row justify-content-center align-items-center">
                    <div className="col-4">
                        <div className="card">
                            <div className="card-body">
                                <form action="" autoComplete="off">
                                    <div className="form-group">
                                        <input type="text" className="form-control" ref={username => this.username = username} placeholder="Username" />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" className="form-control" ref={password => this.password = password} placeholder="Password" />
                                    </div>
                                    <button type="button" onClick={this.login} className="btn btn-primary col-12">Login</button>
                                    <br />
                                    <label ref={err => this.err = err} />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;