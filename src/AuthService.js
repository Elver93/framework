/**
 * Service class for authenticating users against an API
 * and storing JSON Web Tokens in the browsers LocalStorage.
 */
var jwtDecode = require('jwt-decode')

class AuthService {

    constructor(auth_api_url) {
        this.auth_api_url = auth_api_url;
        this.fetch = this.fetch.bind(this);
        this.login = this.login.bind(this);
    }

    login(username, password) {
        return this.fetch(this.auth_api_url, {
            method: 'POST',
            body: JSON.stringify({
                username,
                password
            })
        }).then(res => {
            if (res.token !== undefined)
                this.setToken(res.token);
            return Promise.resolve(res);
        })
    }

    loggedIn() {

        if (this.getToken() !== null && this.getToken() !== undefined)
        {
            if (jwtDecode(this.getToken()).exp < Date.now() / 1000) {
                this.logout();
                console.log('token expired');
            }
        }
        return (this.getToken() !== undefined && this.getToken() != null);
    }

    setToken(token) {
        localStorage.setItem('token', token)
    }

    getToken() {
        return localStorage.getItem('token')
    }

    logout() {
        localStorage.removeItem('token');
    }

    getUserInfo()
    {
        return jwtDecode(this.getToken());
    }

    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
        .then(response => response.json());
    }
}

export default AuthService;
