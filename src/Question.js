import React, { Component } from 'react';

class Question extends Component {
    constructor(props) {
        super(props);

        this.props.questionGet(this.props.id);
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        if (this.props.Auth.loggedIn())
        {
            let add = {
                answer: this.answer.value,
            };

            this.props.answerAdd(add, this.props.id)
            this.answer.value = "";
        }
        else{
            this.err.innerHTML = "You are not logged in";
        }

    };

    handleVote =(event) =>{
        let answerId = event.target.id;

        this.props.answerVote(this.props.question._id, answerId)
    }

    render() {

        let answers = [];
        if(this.props.question.answer)
        {
            this.props.question.answer.forEach((answer, index) => {
                answers.push(
                    <>
                        <div className="row border-top">
                            <div className="col-12">
                                <p className="float-left">Answer by: <b>{answer.username}</b></p>  <p className="float-right"> {answer.date}</p>
                            </div>
                        </div>
                        <div className="row margbot">
                            <div className="col-1 arrowdiv">
                                <button id={answer._id} onClick={this.handleVote}>&#8657;</button>
                                <p>{answer.vote}</p>
                            </div>
                            <div className="col-11">
                                {answer.answer}
                            </div>
                        </div>
                    </>
                )
            })
        }

        return (
            <div className="container cont">
                <h1>{this.props.question.title}</h1>
                <div className="col-12 qTop">
                <p>{this.props.question.date}</p>
                <p>Question By: <b>{this.props.question.username}</b></p>
                </div>
                <div className="row margbot">
                    <div className="col-1 arrowdiv">
                        <p>{this.props.question.views}</p>
                        <p>Views</p>
                    </div>
                    <div className="col-11">


                        the specific
                    </div>
                </div>
                {answers}
                <div className="container-fluid">
                    <br />
                    <textarea  className="form-control" ref={answer => this.answer = answer} rows="3" placeholder="Give your answer to the question" />
                    <br />
                    <button type="button" onClick={this.handleFormSubmit} className="btn btn-primary col-2">Answer</button>
                    <labal ref={err => this.err = err} />
                </div>
            </div>
        );
    }
}

export default Question;