import React, { Component } from 'react';

class Add extends Component {
    constructor(props) {
        super(props);

    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        if (this.props.Auth.loggedIn())
        {
            let add = {
                title: this.title.value,
                question: this.question.value
            };
            this.props.questionAdd(add)
            return this.props.history.push(`/home`)
        }
        else{
            this.err.innerHTML = "You are not logged in";
        }
    };

    render() {
        return (
            <div className="container cont">
                <h1>New Question</h1>

                <div className="row justify-content-center align-items-center">
                    <div className="col-8">
                        <div className="card">
                            <div className="card-body">
                                <form action="" autoComplete="off">
                                    <div className="form-group">
                                        <input type="text" className="form-control" ref={title => this.title = title} placeholder="Question" />
                                    </div>
                                    <div className="form-group">
                                        <textarea  className="form-control" rows="5" ref={question => this.question = question} placeholder="More specific about your question" />
                                    </div>
                                    <button type="button" onClick={this.handleFormSubmit} className="btn btn-primary col-12">Create Question</button>
                                    <labal ref={err => this.err = err} />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Add;