import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch, NavLink, Redirect } from "react-router-dom";
import './App.css';

class Home extends Component {
    constructor(props) {
        super(props);

    }

    addView = (e) =>{
        e.preventDefault()

        let id = e.target.id;

        this.props.questionView(id);
        this.props.history.push("/question/" + id)
    }

    render() {
        return (
            <div className="container cont">
                <h1 className="float-left">Latest Question</h1>
                <NavLink className="btn btn-outline-success float-right" to="/add">New Question</NavLink>

                <table className="tb">
                    <tbody>
                    {this.props.questions.map((question, index) =>
                        <tr key={index}>
                            <td className="tdspan">{question.views}<p>Views</p></td>
                            <td className="tdspan">{question.answer.length}<p>Answers</p></td>
                            <td className="tdfull">
                                <Link onClick={this.addView} id={question._id} to={"/question/" + question._id} >{question.title}</Link>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Home;