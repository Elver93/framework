const path = require('path');
const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const checkJwt = require('express-jwt');
const pathToRegexp = require('path-to-regexp');
const port = (process.env.PORT || 3001);
const app = express();

let corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
}

app.use(bodyparser.json());
app.use(cors(corsOptions));
app.use(morgan('combined'));
app.use(express.static(path.join(__dirname, '../build')));

//mongoose.connect('mongodb://dbuser:niels2502@192.168.1.142/mandatory_m?authSource=admin', {useNewUrlParser: true});
mongoose.connect('mongodb://dbuser:niels2502@5.186.60.140/mandatory_m?authSource=admin', {useNewUrlParser: true});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected to Mongodb")
});

if (!process.env.JWT_SECRET){
    console.error('Missing Secret JWT_SECRET');
    process.exit(1);
}


let openPath = function(req){
    if (req.path === '/api/users/authenticate') return true;
    else if (req.path === '/api/users' && req.method === 'POST') return true;
    else if (req.method === 'GET')
    {
        if (req.path === '/api/question')return true;
        else if (pathToRegexp('/api/question/:id').test(req.path))return true;
        else if (req.path === '/home')return true;
        else if (req.path === '/login')return true;
        else if (req.path === '/add')return true;
        else if (req.path === '/reg')return true;
        else if (pathToRegexp('/question/:id').test(req.path))return true;
        else if (req.path === '/')return true;

    }
    else if (req.method === 'PUT')
    {
        if (pathToRegexp('/api/question/:id').test(req.path))return true;
        else if (pathToRegexp('/api/question/:id/:answerid').test(req.path))return true;
    }
    return false;
};
app.use(checkJwt({secret:process.env.JWT_SECRET}).unless(openPath));
app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError'){
        res.status(401).json({ Error: err.message })
    }
});


let User = require('./User')(mongoose);
let Question = require('./Question')(mongoose);
app.use('/api/users', User);
app.use('/api/question', Question);


app.use((err, req, res, next) =>{
   console.error(err.stack);
   res.status(500).send({msg: 'Something Broke'})
});

/**** Reroute all unknown requests to the React index.html ****/
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.listen(port, () => console.log(`Api running on port ${port}!`));