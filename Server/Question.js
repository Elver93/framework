module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    let AnswerSchema = mongoose.Schema({
        date: String,
        username: String,
        answer: String,
        vote: Number
    });

    let questionSchema = mongoose.Schema({
        title: String,
        question: String,
        date: String,
        vote: Number,
        username: String,
        answer: [AnswerSchema],
        solved: Boolean,
        views: Number
    });

    let questionModel = mongoose.model('question', questionSchema);

    router.get('/', (req,res) => {
        questionModel.find((err, questions) => {
            if (err) return console.log(err);
            res.json(questions);
        }).sort({date: -1})
    });

    router.get('/:id', (req,res) => {
        questionModel.findOne({_id: req.params.id}, (err, question) => {
            if (err) return console.log(err);
            res.json(question);
        });
    });

    router.post('/', (req,res) => {
        let date = new Date();
        date.setUTCHours(date.getUTCHours() + 2)
        let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds()
        let newPost = {
            title: req.body.title,
            question: req.body.question,
            username: req.user.username,
            date: datestring,
            solved: false,
            views: 0,
            vote: 0
        };
        questionModel.create(newPost);
        res.json({msg: `New post have been created`});
    });

    router.post('/:id', (req,res) => {
        let date = new Date();
        date.setUTCHours(date.getUTCHours() + 2)
        let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds()
        console.log(req.params.id)
        questionModel.findById(req.params.id, (err, question) => {

            if (question){
                question.answer.push({
                    username: req.user.username,
                    answer: req.body.answer,
                    date: datestring,
                    vote: 0
                });
                question.save();
                res.json({msg: "Posted an answer "});
            }
            else
            {
                console.log(res.json("Post not found"))
            }
        })
    });

    router.put('/:id', (req,res) => {
        questionModel.findById(req.params.id, (err, question) => {
            if (err) return console.log(err);
            ++question.views;
            question.save()
            res.json("new vote count");
        });
    });


    router.put('/:id/:answerid', (req,res) => {
        questionModel.findById(req.params.id, (err, question) => {
            if (err) return console.log(err);
            let doc = question.answer.id(req.params.answerid);
            ++doc.vote;
            question.save()
            res.json("new vote count");
        });
    });

    router.delete('/:id', (req,res) => {
        res.json({msg: 'Not completed'})
    });

    router.delete('/:id/:answerid', (req,res) => {
        res.json({msg: 'Not completed'})
    });


    return router;
}