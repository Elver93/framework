module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();
    const bcrypt = require('bcrypt');
    const jwt = require('jsonwebtoken');

    let userSchema = mongoose.Schema({
        username: String,
        password: String,
        mail: String,
        rank: Number,
        active: Boolean
    });
    let userModel = mongoose.model('user', userSchema);

    router.post('/', (req,res) => {
        console.log("tmp");
        bcrypt.hash(req.body.password, 10, function(err, hash) {
            console.log(`Hash generated for ${req.body.username}`, hash);

            let newUser = {
                username: req.body.username,
                password: hash,
                mail: req.body.mail,
                rank: 1,
                active: true
            }
            userModel.create(newUser);
        });
        res.json({msg: `A mail have been sent to ${req.body.email} you need to active your account before use`});
    });

    router.post('/authenticate', (req,res) => {
        const username = req.body.username;
        const password = req.body.password;

        if (!username || !password){
            let msg = "Username or password missing!";
            console.error(msg);
            res.status(401).json({msg: msg});
            return;
        }

        userModel.findOne({ username: username }, (err, user) =>{
            if (user){
                bcrypt.compare(password, user.password,(err, result) =>{
                    if (result) {
                        const payload ={
                            username: username,
                            admin: false
                        }
                        const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h'});
                        res.json({
                            msg: "User authenticated successfully",
                            token: token
                        })
                    }
                });
            } else {
                res.status(404).json({ msg: "User not found" + err });
            }
        })
    });

    router.put('/:id', (req,res) => {
        userModel.findOne({_id: req.params.id}).exec((err, user) => {
            if (err)console.log(err);
            if (user){
                user.username = req.body.username;
                user.password = req.body.username;
                user.mail = req.body.mail;
                user.rank = req.body.username;
                user.active = req.body.username;
                user.save()
                res.json({msg: "Updated" + user});
            }
            else
            {
                console.log(res.json("not found"))
            }
        })
    });

    router.delete('/:id', (req,res) => {
        if(req.body.rank === 1){
        userModel.findOneAndRemove({ _id: req.body._id}, (err, user) => {
            if (err) return console.log(err);
            res.json({msg: "Deleted" + user});
        });
        } else {
            res.json({msg: "your not Admin"});
        }
    });

    return router;
}